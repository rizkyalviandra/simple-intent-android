package com.example.balado.bnspsoal1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText txtNama, txtEmail, txtNoHp;
    Button btnSimpan;
    String nama,email,noHp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNama = findViewById(R.id.txtNama);
        txtEmail = findViewById(R.id.txtEmail);
        txtNoHp = findViewById(R.id.txtNoHp);
        btnSimpan = findViewById(R.id.btnSimpan);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nama = txtNama.getText().toString();
                email = txtEmail.getText().toString();
                noHp = txtNoHp.getText().toString();

                Intent i = new Intent(getApplicationContext(),Hasil.class);
                i.putExtra("nama", nama);
                i.putExtra("email",email);
                i.putExtra("noHp",noHp);

                startActivity(i);
            }

        });

    }
}
