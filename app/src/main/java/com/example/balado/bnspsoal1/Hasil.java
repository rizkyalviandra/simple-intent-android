package com.example.balado.bnspsoal1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Hasil extends AppCompatActivity {

    TextView nama, email, noHp;
    String txtNama, txtEmail,txtHp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);

        nama = findViewById(R.id.nama);
        email = findViewById(R.id.email);
        noHp = findViewById(R.id.noHp);

        Intent intent = getIntent();
        txtNama = intent.getStringExtra("nama");
        txtEmail = intent.getStringExtra("email");
        txtHp = intent.getStringExtra("noHp");

        nama.setText(txtNama);
        email.setText(txtEmail);
        noHp.setText(txtHp);
    }
}
